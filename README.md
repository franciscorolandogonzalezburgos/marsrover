
# Mars Rover

This is a Springboot microservice, to run this app you must have instaled java 11 and maven.
clone this repo 

`git clone https://gitlab.com/franciscorolandogonzalezburgos/marsrover.git`

and go to the cloned folder

`cd mars-rover`

For test run

`mvn clean test`

For compile

`mvn clean compile`

To run in your machine run

`mvn spring-boot:run`

and visit http://localhost:8001/ to test the application
