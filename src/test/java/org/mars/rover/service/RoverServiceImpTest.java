package org.mars.rover.service;

import org.junit.jupiter.api.Test;
import org.mars.rover.entity.Plateau;
import org.mars.rover.entity.RoverInstruction;
import org.mars.rover.enums.OrientationEnum;

import java.util.ArrayList;
import java.util.List;

public class RoverServiceImpTest {

    RoverServiceImp service = new RoverServiceImp();

    @Test
    public void moveRovers_ok_case1() {

        Plateau p = new Plateau();
        p.setHeigth(5);
        p.setWidth(5);

        List<RoverInstruction> ril = new ArrayList<>();
        RoverInstruction testCase1 = new RoverInstruction();
        testCase1.setRoverOrientation(OrientationEnum.N);
        testCase1.setInitialPosition(new int[] {1,2});
        testCase1.setIsntructions(new char[] {'L','M','L','M','L','M','L','M','M'});
        testCase1.setSequence(0);

        RoverInstruction testCase2 = new RoverInstruction();
        testCase2.setRoverOrientation(OrientationEnum.E);
        testCase2.setInitialPosition(new int[] {3,3});
        testCase2.setIsntructions(new char[] {'M','M','R','M','M','R','M','R','R','M'});
        testCase2.setSequence(1);

        ril.add(testCase1);
        ril.add(testCase2);

        List<String> result = service.moveRovers(p,ril);

        List<String> expectedResult = new ArrayList<>();
        expectedResult.add("1 3 N");
        expectedResult.add("5 1 E");

        assert(result.size()==2);
        assert(result.contains(expectedResult.get(0)));
        assert(result.contains(expectedResult.get(1)));

    }

}
