package org.mars.rover.service;

import org.junit.jupiter.api.Test;
import org.mars.rover.entity.Plateau;
import org.mars.rover.entity.RoverInstruction;
import org.mars.rover.enums.OrientationEnum;

import java.util.List;

public class FileServiceImpTest {

    FileServiceImp service = new FileServiceImp();

    @Test
    public void getPlateau_ok() {
        byte[] input = "5 5".getBytes();
        Plateau p = service.getPlateauFromFile(input);
        assert(p.getWidth()==5);
        assert(p.getHeigth()==5);
    }

    @Test
    public void getRoverInstruction_ok() {
        byte[] input = "5 5\n1 2 N\nLMLMLMLMM".getBytes();
        List<RoverInstruction> r = service.getInstructionsFromFile(input);
        assert(r.get(0).getInitialPosition()[0] == 1);
        assert(r.get(0).getInitialPosition()[1] == 2);
        assert(r.get(0).getRoverOrientation() == OrientationEnum.N);
    }

}
