package org.mars.rover.entity;

import lombok.Data;
import org.mars.rover.enums.OrientationEnum;

@Data
public class RoverInstruction {
    private int sequence;
    private int[] roverPosition;
    private OrientationEnum roverOrientation;
    private int[] initialPosition;
    private char[] isntructions;
}
