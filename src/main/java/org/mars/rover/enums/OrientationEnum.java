package org.mars.rover.enums;

import org.mars.rover.constants.RoverConstants;
import org.mars.rover.exception.RoversException;

public enum OrientationEnum {
    N(RoverConstants.NORTH_CHAR,"North"),
    S(RoverConstants.SOUTH_CHAR,"South"),
    E(RoverConstants.EAST_CHAR,"East"),
    W(RoverConstants.WEST_CHAR,"West");

    private final char orientation;
    private final String name;

    private OrientationEnum(char o, String n) {
        this.orientation = o;
        this.name = n;
    }

    public char getOrientation(){
        return this.orientation;
    }

    public static OrientationEnum getOrientation(String n) {
        for(OrientationEnum o:OrientationEnum.values()) {
            if(o.toString().equalsIgnoreCase(n)){
                return o;
            }
        }
        throw new RoversException("Error, bad Orientation");
    }

    public String getName(){
        return this.name;
    }

    public OrientationEnum turnRigth(){
        switch(this.orientation){
            case RoverConstants.NORTH_CHAR: return OrientationEnum.valueOf(RoverConstants.EAST_STRING);
            case RoverConstants.EAST_CHAR: return OrientationEnum.valueOf(RoverConstants.SOUTH_STRING);
            case RoverConstants.SOUTH_CHAR: return OrientationEnum.valueOf(RoverConstants.WEST_STRING);
            case RoverConstants.WEST_CHAR: return OrientationEnum.valueOf(RoverConstants.NORTH_STRING);
            default: throw new RoversException("Orientation error");
        }
    }

    public OrientationEnum turnLeft() {
        switch(this.orientation){
            case RoverConstants.NORTH_CHAR: return OrientationEnum.valueOf(RoverConstants.WEST_STRING);
            case RoverConstants.WEST_CHAR: return OrientationEnum.valueOf(RoverConstants.SOUTH_STRING);
            case RoverConstants.SOUTH_CHAR: return OrientationEnum.valueOf(RoverConstants.EAST_STRING);
            case RoverConstants.EAST_CHAR: return OrientationEnum.valueOf(RoverConstants.NORTH_STRING);
            default: throw new RoversException("Orientation error");
        }
    }
}
