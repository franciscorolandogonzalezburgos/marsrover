package org.mars.rover.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mars.rover.entity.Plateau;
import org.mars.rover.entity.RoverInstruction;
import org.mars.rover.exception.RoversException;
import org.mars.rover.service.FileService;
import org.mars.rover.service.RoverService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.List;

@Slf4j
@Controller
@RequiredArgsConstructor
public class WelcomeController {

    private final FileService fileService;
    private final RoverService roverService;

    @GetMapping("/")
    public String getHome() {
        return "index";
    }

    @PostMapping("/instructions")
    public String uploadFile(@RequestParam("file") MultipartFile file, Model model){

        try{
            if(null == file){
                throw new RoversException("Error, file is empty",500);
            }

            List<String> result = inputRoverFile(file);

            StringBuilder sb = new StringBuilder();

            result.stream().forEach(s->sb.append(String.format("%s\n",s)));

            byte[] byteFile = sb.toString().getBytes();

            model.addAttribute("success", true);
            model.addAttribute("result", result);

            log.info("success");

            return "result";
        }catch(RoversException re){

            log.info("error");

            model.addAttribute("success", false);
            model.addAttribute("errorMessage", re.getMessage());
            return "result";
        }
    }

    private List<String> inputRoverFile(MultipartFile file) {
        try{

            Plateau p = fileService.getPlateauFromFile(file.getBytes());
            List<RoverInstruction> ri = fileService.getInstructionsFromFile(file.getBytes());

            return roverService.moveRovers(p,ri);

        }catch(IOException io) {
            throw new RoversException("Error, invalid input", 400, io);
        }catch(RoversException re){
            if(re.getCode()==0)
                throw new RoversException(re.getMessage(),500);
            else
                throw re;
        }catch(Exception e){
            throw new RoversException("Error, unknown error", 500, e);
        }
    }
}
