package org.mars.rover.service;

import lombok.extern.slf4j.Slf4j;
import org.mars.rover.constants.RoverConstants;
import org.mars.rover.entity.Plateau;
import org.mars.rover.entity.RoverInstruction;
import org.mars.rover.exception.RoversException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class RoverServiceImp implements RoverService {

    @Override
    public List<String> moveRovers(Plateau plateau, List<RoverInstruction> instruction) {

        if(instruction.isEmpty()){
            return new ArrayList<>();
        }

        return instruction.
                stream().
                filter(i->validateInitialPosition(i,plateau)).
                map(i->applyInstruction(i,plateau,0)).
                collect(Collectors.toList());

    }

    private boolean validateInitialPosition(RoverInstruction ri, Plateau p){
        return ri.getInitialPosition()[0] <= p.getWidth() && ri.getInitialPosition()[1] <= p.getHeigth();
    }

    private String applyInstruction(RoverInstruction ri, Plateau p, int i) {

        if (i >= ri.getIsntructions().length)
            return String.format("%d %d %c",ri.getRoverPosition()[0], ri.getRoverPosition()[1], ri.getRoverOrientation().getOrientation());

        if(i==0){
            ri.setRoverPosition(ri.getInitialPosition());
        }

        switch(ri.getIsntructions()[i]){
            case RoverConstants.LEFT:
                ri.setRoverOrientation(ri.getRoverOrientation().turnLeft());
                break;
            case RoverConstants.RIGTH:
                ri.setRoverOrientation(ri.getRoverOrientation().turnRigth());
                break;
            case RoverConstants.MOVE:
                moveForward(ri.getRoverPosition(), ri.getRoverOrientation().getOrientation(),p);
                break;
            default:
                throw new RoversException("Error, invalid instruction for Rover");
        }
        return applyInstruction(ri,p,++i);
    }

    private void moveForward(int[] p, char o, Plateau pl){
        switch(o){
            case RoverConstants.NORTH_CHAR: if( p[1]+1 <= pl.getHeigth()) p[1]++;
                break;
            case RoverConstants.SOUTH_CHAR: if( p[1]-1 >= 0) p[1]--;
                break;
            case RoverConstants.EAST_CHAR: if( p[0]+1 <= pl.getWidth()) p[0]++;
                break;
            case RoverConstants.WEST_CHAR: if( p[0]-1 >= 0) p[0]--;
                break;
            default: throw new RoversException("Error, invalid orientation to move rover");
        }
    }

}
