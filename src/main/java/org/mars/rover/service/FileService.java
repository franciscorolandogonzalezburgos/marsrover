package org.mars.rover.service;

import org.mars.rover.entity.Plateau;
import org.mars.rover.entity.RoverInstruction;

import java.util.List;

public interface FileService {
    Plateau getPlateauFromFile(byte[] file);
    List<RoverInstruction> getInstructionsFromFile(byte[] file);
}
