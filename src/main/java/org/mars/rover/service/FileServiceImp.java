package org.mars.rover.service;

import org.mars.rover.constants.RoverConstants;
import org.mars.rover.entity.Plateau;
import org.mars.rover.entity.RoverInstruction;
import org.mars.rover.enums.OrientationEnum;
import org.mars.rover.exception.RoversException;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileServiceImp implements FileService{

    @Override
    public Plateau getPlateauFromFile(byte[] file) {
        String[] fileContent = new String(file, StandardCharsets.UTF_8).split("\n");
        String plateauSpec = fileContent[0];

        return validatePlateau(plateauSpec.trim());

    }

    private Plateau validatePlateau(String s) {
        String[] plateauParams = s.split(" ");

        if(plateauParams.length != 2){
            throw new RoversException("Error, invalid plateau spec");
        }

        Plateau p = new Plateau();
        p.setWidth(parsePosition(plateauParams[0]));
        p.setHeigth(parsePosition(plateauParams[1]));

        return p;
    }

    private int parsePosition(String s) {
        try{
            return Integer.parseInt(s);
        }catch(NumberFormatException nfe){
            throw new RoversException("Error, position bad param",400);
        }
    }

    @Override
    public List<RoverInstruction> getInstructionsFromFile(byte[] file) {

        String[] fileContent = new String(file, StandardCharsets.UTF_8).split("\n");

        if(fileContent.length<3 || fileContent.length%2==0){
            throw new RoversException("Error, bad file count", 400);
        }

        int roverCount = 0;

        List<RoverInstruction> instructionList = new ArrayList<>();

        for(int i=1; i < fileContent.length; i+=2){
            instructionList.add(mapToInstruction(fileContent[i],fileContent[i+1],roverCount));
            roverCount++;
        }

        return instructionList;
    }

    private RoverInstruction mapToInstruction(String p, String i, int s) {

        String[] position = p.split(" ");
        char[] instructions =validateInstructions(i.trim());

        RoverInstruction ri = new RoverInstruction();

        ri.setSequence(s);
        ri.setInitialPosition(new int[] {parsePosition(position[0]),parsePosition(position[1])});
        ri.setRoverOrientation(validateOrientation(position[2]));
        ri.setIsntructions(instructions);

        return ri;

    }

    private char[] validateInstructions(String i) {
        try{
            char[] instructions = i.toCharArray();

            for(char c:instructions) {
                switch(c) {
                    case RoverConstants.LEFT:
                    case RoverConstants.RIGTH:
                    case RoverConstants.MOVE:
                        break;
                    default:
                        throw new RoversException("Error, bad instruction", 400);
                }
            }

            return instructions;

        }catch(Exception e){
            throw new RoversException("Error, bad instruction", 400);
        }
    }

    private OrientationEnum validateOrientation(String s){
        try{
            return OrientationEnum.getOrientation(s);
        }catch(RoversException re){
            throw new RoversException(re.getMessage(),400);
        }catch(Exception e){
            throw new RoversException(String.format("Unknow error:{}",e.getMessage()),500);
        }
    }
}
