package org.mars.rover.service;

import org.mars.rover.entity.Plateau;
import org.mars.rover.entity.RoverInstruction;

import java.util.List;

public interface RoverService {
    List<String> moveRovers(Plateau plateau, List<RoverInstruction> instruction);
}
