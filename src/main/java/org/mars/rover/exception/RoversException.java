package org.mars.rover.exception;

public class RoversException extends RuntimeException{
    private final int code;

    public RoversException(){
        super();
        this.code = 0;
    }
    public RoversException(String message) {
        super(message);
        this.code = 0;
    }
    public RoversException(String message, int code){
        super(message);
        this.code = code;
    }
    public RoversException(String message, int code, Throwable e) {
        super(message,e);
        this.code = code;
    }
    public int getCode() {
        return this.code;
    }
}
