package org.mars.rover.constants;

public abstract class RoverConstants {
    public static final char LEFT = 'L';
    public static final char RIGTH = 'R';
    public static final char MOVE = 'M';

    public static final char NORTH_CHAR = 'N';
    public static final char SOUTH_CHAR = 'S';
    public static final char EAST_CHAR = 'E';
    public static final char WEST_CHAR = 'W';
    public static final String NORTH_STRING = "N";
    public static final String SOUTH_STRING = "S";
    public static final String EAST_STRING = "E";
    public static final String WEST_STRING = "W";
}
